from yat.model import *


class ConstantFolder:

    def visit(self, tree):
        return tree.accept(self)

    def visit_number(self, tree_obj):
        return tree_obj

    def visit_function_call(self, tree_obj):
        new_args = list(map(self.visit, tree_obj.args))
        return FunctionCall(tree_obj.fun_expr, new_args)

    def visit_function_def(self, tree_obj):
        new_function = Function(tree_obj.function.args, [])
        new_function.body = list(map(self.visit, tree_obj.function.body))
        return FunctionDefinition(tree_obj.name, new_function)

    def visit_conditional(self, tree_obj):
        new_cond = self.visit(tree_obj.cond)
        new_if_true = list(map(self.visit, tree_obj.if_true))
        new_if_false = list(map(self.visit, tree_obj.if_false))
        return Conditional(new_cond, new_if_true, new_if_false)

    def visit_read(self, tree_obj):
        return tree_obj

    def visit_print(self, tree_obj):
        return Print(self.visit(tree_obj.expr))

    def visit_reference(self, tree_obj):
        return tree_obj

    def visit_bin_op(self, tree_obj):
        lhs = self.visit(tree_obj.lhs)
        rhs = self.visit(tree_obj.rhs)
        if tree_obj.op == '*':
            if type(lhs) == Number:
                if lhs.value == 0:
                    return Number(0)
            if type(rhs) == Number:
                if rhs.value == 0:
                    return Number(0)
        if type(lhs) == type(rhs) == Reference:
            if lhs.name == rhs.name and tree_obj.op == '-':
                return Number(0)
        new_obj = BinaryOperation(lhs, tree_obj.op, rhs)
        if type(lhs) == type(rhs) == Number:
            return new_obj.evaluate(Scope())
        return new_obj

    def visit_un_op(self, tree_obj):
        obj = UnaryOperation(tree_obj.op, tree_obj.expr)
        obj.expr = self.visit(obj.expr)
        if type(obj.expr) == Number:
            return obj.evaluate(Scope())
        return obj
