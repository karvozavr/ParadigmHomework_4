#!/usr/bin/env python3


from yat.model import *
from printer import PrettyPrinter
from folder import ConstantFolder


def example():
    parent = Scope()
    parent['foo'] = Function(('hello', 'world'),
                             [Print(BinaryOperation(Reference('hello'),
                                                    '+',
                                                    Reference('world')))])
    print('It should print 2: ', end=' ')
    FunctionCall(FunctionDefinition('foo', parent['foo']),
                 [Number(5), UnaryOperation('-', Number(3))]).evaluate(parent)


def func_condition_read_test():
    print('Enter a, b, c')
    bin_op = BinaryOperation

    main = Scope()
    main['foo'] = Function(('enum', 'denom', 'val'),
                           [
                               Conditional(
                                   FunctionCall(Reference('check_answer'),
                                                [Reference('enum'),
                                                 Reference('denom'),
                                                 Reference('val')]),
                                   [
                                       Print(Reference('val'))
                                   ],
                                   [
                                       Print(Number(-1))
                                   ]
                               )
                           ])
    main['check_answer'] = Function(('a', 'b', 'c'),
                                    [
                                        Conditional(
                                            bin_op(Reference('c'),
                                                   '==',
                                                   bin_op(Reference('a'),
                                                          '+',
                                                          Reference('b'))),
                                            [
                                                Number(1)
                                            ],
                                            [
                                                Number(0)
                                            ])
                                    ])
    print('It would print c, if a + b = c and -1 if not: ', end=' ')
    assert FunctionCall(FunctionDefinition('foo', main['foo']),
                        [
                            Read('first'),
                            Read('second'),
                            Read('third')
                        ]).evaluate(main)


def scope_test():
    print('This should print 3, 2:')

    main = Scope()
    main['a'] = Number(1)
    main['b'] = Number(2)
    scope = Scope(main)
    scope['a'] = Number(3)
    assert Print(Reference('a')).evaluate(scope).value == 3
    assert Print(scope['b']).evaluate(scope).value == 2
    assert main['a'].value == 1


def func_in_func_test():
    """
    def foo():
        print('I am foo')

    def bar():
        print('I am bar')

    def chose_func(a, b):
        if a > b:
            return foo
        else:
            return bar
    """

    f_def = FunctionDefinition
    main = Scope()
    main['foo'] = Function((),
                           [
                               Number(10)
                           ])
    main['bar'] = Function((),
                           [
                               Number(20)
                           ])
    main['chose_func'] = Function(('a', 'b'),
                                  [
                                      Conditional(
                                          BinaryOperation(
                                              Reference('a'),
                                              '>',
                                              Reference('b')
                                          ),
                                          [
                                              f_def('foo', main['foo'])
                                          ],
                                          [
                                              f_def('bar', main['bar'])
                                          ]
                                      )
                                  ])
    main['a'] = Number(2)
    main['b'] = Number(1)
    main['new_foo'] = FunctionCall(f_def('chose_func', main['chose_func']),
                                   [main['a'], main['b']]).evaluate(main)
    assert FunctionCall(f_def('new_foo', main['new_foo']),
                        []).evaluate(main).value == 10
    main['b'] = Number(3)
    main['new_bar'] = FunctionCall(f_def('chose_func', main['chose_func']),
                                   [main['a'], main['b']]).evaluate(main)
    assert FunctionCall(f_def('new_bar', main['new_bar']),
                        []).evaluate(main).value == 20


def condition_test():
    # if condition is not Number(0) it's true
    main = Scope()
    Conditional(Number(0), []).evaluate(main)


def func_def_test():
    print('This should print 25:')
    main = Scope()
    f_def = FunctionDefinition(
        'foo',
        Function(['arg'],
                 [
                     Print(Reference('arg'))
                 ]
                 )
    )
    assert FunctionCall(f_def, [Number(25)]).evaluate(main).value == 25


def arithm_test():
    ref = Reference
    bin_op = BinaryOperation
    main = Scope()
    print('Enter a and b:')

    Read('a').evaluate(main)
    Read('b').evaluate(main)
    l = ref('a').evaluate(main).value
    r = ref('b').evaluate(main).value

    result = bin_op(ref('a'), '+', ref('b')).evaluate(main).value
    assert l + r == result
    print('{lv}{op}{rv}={res}'.format(lv=l, op='+', rv=r, res=result))

    result = bin_op(ref('a'), '-', ref('b')).evaluate(main).value
    assert l - r == result
    print('{lv}{op}{rv}={res}'.format(lv=l, op='-', rv=r, res=result))

    result = bin_op(ref('a'), '*', ref('b')).evaluate(main).value
    assert l * r == result
    print('{lv}{op}{rv}={res}'.format(lv=l, op='*', rv=r, res=result))

    result = bin_op(ref('a'), '/', ref('b')).evaluate(main).value
    assert l // r == result
    print('{lv}{op}{rv}={res}'.format(lv=l, op='/', rv=r, res=result))

    result = bin_op(ref('a'), '%', ref('b')).evaluate(main).value
    assert l % r == result
    print('{lv}{op}{rv}={res}'.format(lv=l, op='%', rv=r, res=result))

    result = bin_op(ref('a'), '<', ref('b')).evaluate(main).value
    assert int(l < r) == result
    print('{lv}{op}{rv}={res}'.format(lv=l, op='<', rv=r, res=result))

    result = bin_op(ref('a'), '>', ref('b')).evaluate(main).value
    assert int(l > r) == result
    print('{lv}{op}{rv}={res}'.format(lv=l, op='>', rv=r, res=result))

    result = bin_op(ref('a'), '==', ref('b')).evaluate(main).value
    assert int(l == r) == result
    print('{lv}{op}{rv}={res}'.format(lv=l, op='==', rv=r, res=result))

    result = bin_op(ref('a'), '<=', ref('b')).evaluate(main).value
    assert int(l <= r) == result
    print('{lv}{op}{rv}={res}'.format(lv=l, op='<=', rv=r, res=result))

    result = bin_op(ref('a'), '>=', ref('b')).evaluate(main).value
    assert int(l >= r) == result
    print('{lv}{op}{rv}={res}'.format(lv=l, op='>=', rv=r, res=result))

    result = bin_op(ref('a'), '&&', ref('b')).evaluate(main).value
    assert int(l and r) == result
    print('{lv}{op}{rv}={res}'.format(lv=l, op='&&', rv=r, res=result))

    result = bin_op(ref('a'), '||', ref('b')).evaluate(main).value
    assert int(l or r) == result
    print('{lv}{op}{rv}={res}'.format(lv=l, op='||', rv=r, res=result))

    print('arithm OK')


def func_call_test():

    main = Scope()
    main['arg'] = Number(1)
    f_def = FunctionDefinition(
        'foo',
        Function(['arg'],
                 [
                     Reference('arg')
                 ]
                 )
    )
    FunctionCall(f_def, [Number(25)]).evaluate(main)
    assert Reference('arg').evaluate(main).value == 1


def my_tests():
    func_def_test()
    arithm_test()
    func_condition_read_test()
    scope_test()
    func_in_func_test()
    example()
    condition_test()
    func_call_test()


def printer_test():
    bin_op = BinaryOperation
    un_op = UnaryOperation
    prnt_obj = PrettyPrinter()
    folder_obj = ConstantFolder()
    tree = FunctionDefinition(
        'foo',
        Function(['arg', 'org'],
                 [
                     Conditional(
                         Number(10),
                         [
                             bin_op(bin_op(un_op('-', Number(1)),
                                           '+',
                                           Number(10)),
                                    '*',
                                    bin_op(Reference('a'),
                                           '-',
                                           Reference('a')))
                         ],
                         [
                             Conditional(
                                 bin_op(Reference('c'),
                                        '==',
                                        bin_op(Reference('a'),
                                               '+',
                                               Reference('b'))),
                                 [
                                     un_op('-', Number(1))
                                 ],
                                 [
                                     Read('arg')
                                 ])
                         ]
                     )
                 ]
                 )
    )
    print('BEFORE:')
    prnt_obj.visit(tree)
    tree2 = folder_obj.visit(tree)
    print('AFTER:')
    prnt_obj.visit(tree2)
    print('We didnt brake first tree:')
    prnt_obj.visit(tree)
    catch_the_bug_test()


def folder_test():
    folder_obj = ConstantFolder()
    bin_op = BinaryOperation
    printer_obj = PrettyPrinter()
    tree = bin_op(bin_op(Reference('arg'), '*', Number(0)), '+', Number(30))
    printer_obj.visit(tree)
    tree = folder_obj.visit(tree)
    print('Should print "30;":')
    printer_obj.visit(tree)

def catch_the_bug_test():
    f_def = FunctionDefinition
    main = Scope()
    main['foo'] = Function((),
                           [
                               Number(10)
                           ])
    main['bar'] = Function((),
                           [
                               Number(20)
                           ])
    main['chose_func'] = Function(('a', 'b'),
                                  [
                                      f_def('bar', main['bar'])
                                  ])
    main['a'] = Number(2)
    main['b'] = Number(1)
    prnt = PrettyPrinter()
    print('\nCall function from function returning function')
    prnt.visit(FunctionCall(FunctionCall(Reference('choose_func'),
                                   [main['a'], main['b']]), [Number(1), Number(2)]))
    print('\n')

if __name__ == '__main__':
    printer_test()
    folder_test()
    
