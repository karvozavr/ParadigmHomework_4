class PrettyPrinter:

    def __init__(self, indent=''):
        self.indent = indent

    def visit(self, tree):
        print(self.inner_visit(tree) + ';')

    def inner_visit(self, tree):
        return tree.accept(self)

    def visit_number(self, obj):
        return str(obj.value)

    def visit_function_call(self, obj):
        ret_str = '{func}({args})'
        ret_str = ret_str.format(func=self.inner_visit(obj.fun_expr),
                                 args=', '.join([self.inner_visit(tree)
                                                 for tree in obj.args]))
        return ret_str

    def visit_function_def(self, obj):
        self.indent += '\t'
        f_args = obj.function.args
        f_body = obj.function.body
        ret_str = 'def {func}({args}){{\n{ops}{ind}}}'
        ret_str = ret_str.format(func=obj.name,
                                 args=', '.join(f_args),
                                 ops=self.visit_op_list(f_body),
                                 ind=self.indent[:-1])
        self.indent = self.indent[:-1]
        return ret_str

    def visit_op_list(self, op_list):
        return ''.join([self.indent +
                        self.inner_visit(tree) +
                        ';\n'
                        for tree in op_list or []])

    def visit_conditional(self, obj):
        self.indent += '\t'
        ret_str = 'if ({cond}){{\n{if_true}{ind}}} else {{\n{if_false}{ind}}}'
        ret_str = ret_str.format(cond=self.inner_visit(obj.cond),
                                 if_true=self.visit_op_list(obj.if_true),
                                 if_false=self.visit_op_list(obj.if_false),
                                 ind=self.indent[:-1])
        self.indent = self.indent[:-1]
        return ret_str

    def visit_read(self, obj):
        return 'read ' + obj.name

    def visit_print(self, obj):
        return 'print ' + self.inner_visit(obj.expr)

    def visit_reference(self, obj):
        return obj.name

    def visit_bin_op(self, obj):
        ret_str = '({lhs} {op} {rhs})'
        ret_str = ret_str.format(lhs=self.inner_visit(obj.lhs),
                                 op=obj.op,
                                 rhs=self.inner_visit(obj.rhs))
        return ret_str

    def visit_un_op(self, obj):
        ret_str = '{op}{rhs}'
        ret_str = ret_str.format(op=obj.op,
                                 rhs=self.inner_visit(obj.expr))
        return ret_str
